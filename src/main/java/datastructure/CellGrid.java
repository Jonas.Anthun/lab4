package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    CellState initialState;
    CellState [][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows; 
        this.columns = columns; 
        this.initialState = initialState; 
        grid = new CellState[rows][columns];
        for (int row=0; row<rows; row++){
            for (int column=0; column<columns; column++){
                grid[row][column]=initialState;
            }
        } 
	}

    @Override
    public int numRows() {
        return grid.length;
    }

    @Override
    public int numColumns() {
        return grid[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
    }
        if (column < 0 || column>=numColumns()){
        throw new IndexOutOfBoundsException();


        }
        grid[row][column] = element; 
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column>=numColumns()){
            throw new IndexOutOfBoundsException();
        }    
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridTwo= new CellGrid(numRows(), numColumns(), initialState);
        for (int row=0; row<rows; row++){
            for (int column=0; column<columns; column++){
                gridTwo.set(row,column, get(row, column));
            }
        }
        return gridTwo;
    }
    
}